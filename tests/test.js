const assert = require('chai').assert;

function add(a, b) {
  return a + b;
}

describe('add', function() {
  it('devrait retourner la somme de deux nombres', function() {
    assert.equal(add(2, 3), 5);
  });
});

