## Smart contract « Factory de token FA2.1  »

### Team

    - Louis Deloffre
    - Romain Morel
    - Lenaïg Denys
    - Justinien Niemczycki

## Contrat 

Contrat disponible ici : https://better-call.dev/ghostnet/KT1Jrg6G9ziKbpSvzPLhjM4pXWYJqxoZdhMZ/interact/

- Adresse : KT1Jrg6G9ziKbpSvzPLhjM4pXWYJqxoZdhMZ

### Entrypoint
    - burn
    - createContract
    - transfer

### View
    - get_balance
    - get_token_metadata
